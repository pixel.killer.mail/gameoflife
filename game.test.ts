import { Grid } from './Grid'

describe('Grid', () => {
    it('returns position status', () => {
        const grid = [{position: 1, status: 1}]
        const position = 1
        const gridStatus = new Grid(grid).status(position)

        expect(gridStatus).toEqual({position: 1, status: 1})
    })

    it('returns give a postion if the cell is alive', () => {
        const grid = [{position: 1, status: 1}]
        const position = 1
        const alive = 1
        const gridStatus = new Grid(grid).status(position)

        expect(gridStatus?.status).toEqual(alive)
    })

    it('returns alive cell if has two cells alive around', () => {
        const grid = [{position: 1, status: 0}, {position: 2, status: 1}, {position: 3, status: 0}, {position: 4, status: 1}, {position: 5, status: 0}, {position: 6, status: 0}, {position: 7, status: 0}, {position: 8, status: 0}, {position: 9, status: 0}]
        const position = 1
        const alive = 1
        const neighbours = [2, 4]

        const result = new Grid(grid).areYouAlive(position, neighbours)

        expect(result).toEqual({position: position, status: alive})
    })

    it('returns dead cell if has more than 3 cells alive around', () => {
        const grid = [{position: 1, status: 1}, {position: 2, status: 1}, {position: 3, status: 0}, {position: 4, status: 1}, {position: 5, status: 0}, {position: 6, status: 0}, {position: 7, status: 0}, {position: 8, status: 1}, {position: 9, status: 0}]
        const position = 1
        const dead = 0
        const neighbours = [2, 4, 1, 8]

        const result = new Grid(grid).areYouAlive(position, neighbours)

        expect(result).toEqual({position: position, status: dead})
    })
})

