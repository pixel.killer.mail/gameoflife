export class Grid {
    grid: {position: number, status: number}[] = [{position: 1, status: 0}, {position: 2, status: 0}, {position: 3, status: 0}, {position: 4, status: 0}, {position: 5, status: 0}, {position: 6, status: 0}, {position: 7, status: 0}, {position: 8, status: 0}, {position: 9, status: 0}]

    constructor(grid: {position: number, status: number}[]) {
        this.grid = grid
    }

    size():number {
        return this.grid.length
    }

    status(position: number): {position: number, status: number} | undefined {
       return this.grid.find((element) => {
            return element.position === position
        })
    }

    areYouAlive(position: number, neighbours: number[]): object | undefined {
        const alives = neighbours.map((element) => {
            if(this.grid[element].status === 1) {
                return element
            }
        })

        if(alives.length > 3) {
            return {position: position, status: 0}
        } else {
            return {position: position, status: 1}
        }
    }
}